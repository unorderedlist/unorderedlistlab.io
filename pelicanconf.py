#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Dhruv Suthar"
SITENAME = "\u00B7\u2000Unordered List\u2000\u00B7"
SITEURL = ""

PATH = "content"
OUTPUT_PATH = "public"

TIMEZONE = "UTC"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
# SOCIAL = (
#     ('github', 'https://github.com/unorderedlist'),
# )

DEFAULT_PAGINATION = 8

THEME = "template"

STATIC_PATHS = ["images", "extra"]

CUSTOM_CSS_URL = '/static/custom.css'

# set url of custom.css to EXTRA_PATH_METADATA.
EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
}

TYPOGRIFY = True

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'

ARCHIVES_SAVE_AS = 'posts/index.html'
ARCHIVES_URL = "posts"

ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}'
